//
//  InfoAreaSectorViewController.swift
//  Steward Check-in
//
//  Created by Giacomo Boemio on 02/11/21.
//  Copyright © 2021 Bluenet srl. All rights reserved.
//

import Foundation
import UIKit

class InfoAreaSectorViewController: UITableViewController {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    var strArea : String = ""
    var arrAreasAndSectorsLocal = [String : [String]]()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = NSLocalizedString("areas_sectors_selected", comment: "")
        let image = UIImage(systemName: "multiply.circle")
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: image, style:.plain, target: self, action:#selector(closeView))

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrAreasAndSectorsLocal.keys.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let key = Array(self.arrAreasAndSectorsLocal.keys)[section]
        return key
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let arr = Array(self.arrAreasAndSectorsLocal.values)[section]
        if(arr.count > 1){
            return arr.count
        }else{
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        let key = Array(self.arrAreasAndSectorsLocal.keys)[indexPath.section]
        let arr = self.arrAreasAndSectorsLocal[key]
        if(arr!.count > 0){
            let value = arr![indexPath.row]
            cell.textLabel?.text = value
        }else{
            cell.textLabel?.text = NSLocalizedString("no_sectors", comment: "")
        }
        cell.textLabel?.textColor = .black
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    }



}

