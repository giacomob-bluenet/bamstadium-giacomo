//
//  CityViewController.swift
//  Steward Check-in
//
//  Created by Youssef on 08/10/2019.
//  Copyright © 2019 KM, Abhilash. All rights reserved.
//

import UIKit

// MARK: - CityViewController Delegate

protocol CityViewControllerDelegate
{
    func CityViewControllerResponse(city: String)
}

// MARK: - CityViewController

class CityViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    
    // MARK: - Variebles

    var Cities : [City]?
    var filterCities : [City]?
    let searchController = UISearchController(searchResultsController: nil)
    var delegate: CityViewControllerDelegate?
    
    // MARK: - IBOutlet

    @IBOutlet weak var cityTableView: UITableView!
    
    
    // MARK: - ViewController methods

    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup the Search Controller
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Cerca.."
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        cityTableView.dataSource = self
        cityTableView.delegate = self
        
        // Set Navigation Bar Item
        navigationItem.title = NSLocalizedString("cityField", comment: "")
        // fix the searchbar to the top
        if #available(iOS 11.0, *) {
            navigationItem.hidesSearchBarWhenScrolling = false
        }
    }
    
}

// MARK: - Table View Methods


extension CityViewController {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering() {
            return filterCities?.count ?? 0
        }
          
        return Cities?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "Cell")
        var city : City?
        if isFiltering() {
            city = filterCities![indexPath.row]
        } else {
            city = Cities![indexPath.row]
        }
        
        cell.textLabel?.text = city?.cm
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 17)
        cell.textLabel?.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        cell.detailTextLabel?.text = city?.pv
        cell.detailTextLabel?.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
           
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var city : City?
        if isFiltering() {
            city = filterCities![indexPath.row]
        } else {
            city = Cities![indexPath.row]
        }
        self.delegate?.CityViewControllerResponse(city: city?.cm ?? "")
        self.navigationController?.popViewController(animated: true)
    }
    

}

// MARK: - UISearchResultsUpdating

extension CityViewController: UISearchResultsUpdating {
    
    func isFiltering() -> Bool {
         return searchController.isActive && !searchBarIsEmpty()
    }
    
   // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
      filterContentForSearchText(searchController.searchBar.text!)
    }
    
    // MARK: - Private instance methods
      
    func searchBarIsEmpty() -> Bool {
      // Returns true if the text is empty or nil
      return searchController.searchBar.text?.isEmpty ?? true
    }
      
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        filterCities = Cities!.filter({( city : City) -> Bool in
            //Used hasPrefix instead of contains to display the city that begins with the characters entered
            return (city.cm.lowercased().hasPrefix(searchText.lowercased()))
        })

      cityTableView.reloadData()
    }
}
