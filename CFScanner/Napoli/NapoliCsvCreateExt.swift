//
//  LazioCsvCreateExt.swift
//  Steward Check-in lazio
//
//  Created by Alessandro de Peppo on 04/02/2020.
//  Copyright © 2020 Bluenet. All rights reserved.
//

import Foundation

extension CsvCreate {
    
    class func getHeader() -> String {
            let header = "\(NSLocalizedString("surnameField", comment: "" ))"
                + ";" + "\(NSLocalizedString("nameField", comment: "" ))"
                + ";" + "\(NSLocalizedString("dobField", comment: "" ))"
                + ";" + "\(NSLocalizedString("cityField", comment: "" ))"
                + ";" + "\(NSLocalizedString("cfField", comment: "" ))"
                + ";" + "\(NSLocalizedString("CheckedTime", comment: "" ))"
                + ";" + "\(NSLocalizedString("Presenza", comment: "" ))\n"
        return header
    }
    
    class func getFormattedRow(for item : Steward) -> String {
        let dob = DateFormatter.df.string(for: item.dob)
        let checkedTime = DateFormatter.dtf.string(for: item.checkTime)
        let row = "\(item.familyName ?? "")"
            + ";" + "\(item.givenName ?? "")"
            + ";" + "\(dob ?? "")"
            + ";" + "\(item.city ?? "")"
            + ";" + "\(item.cf ?? "")"
            + ";" + "\(checkedTime ?? "")"
            + ";" + "1\n"
       
        return row;
    }
}
