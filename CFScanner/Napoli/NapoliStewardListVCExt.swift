//
//  NapoliStewardLVCExt.swift
//  Steward Check-in
//
//  Created by Youssef on 05/02/2020.
//  Copyright © 2020 KM, Abhilash. All rights reserved.
//

import Foundation

extension StewardListViewController {
    internal func setDetailLabel(steward: Steward) -> String? {
        return steward.cf 
    }
    
    internal func filterResults(steward: Steward,searchText: String) -> Bool {
        
        if (steward.familyName != nil && steward.givenName != nil){
            return (steward.familyName!.lowercased().hasPrefix(searchText.lowercased()) || steward.cf!.lowercased().hasPrefix(searchText.lowercased()) || steward.givenName!.lowercased().hasPrefix(searchText.lowercased()))
            
        }else{
            return (steward.cf!.lowercased().hasPrefix(searchText.lowercased()))
        }
    }
    
  
    
}
