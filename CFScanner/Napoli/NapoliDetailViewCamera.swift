//
//  NapoliDetailVIewCamera.swift
//  Steward Check-in
//
//  Created by Nicola on 27/10/21.
//  Copyright © 2021 KM, Abhilash. All rights reserved.
//

import UIKit

protocol DetailViewCameraDelegate: class {
    func confirmButton()
}

public class DetailViewCamera: UIView {
    
    @IBOutlet var contentViewCamera: UIView!
    @IBOutlet weak var confButton: UIButton!
    @IBOutlet weak var nome: UILabel!
    @IBOutlet weak var checked: UILabel!
    @IBOutlet weak var dob: UILabel!
    @IBOutlet weak var cf: UILabel!
    @IBOutlet weak var cognome: UILabel!
    @IBOutlet weak var row: UILabel!
    @IBOutlet weak var seat: UILabel!
    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet weak var lblCognome: UILabel!
    @IBOutlet weak var lblNome: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblFila: UILabel!
    @IBOutlet weak var lblPosto: UILabel!

    weak var delegate : DetailViewCameraDelegate?

    override public init(frame: CGRect)
    {
        super.init(frame: frame)
        self.commonInit()
    }

    required public init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        self.commonInit()
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("NapoliDetailViewCamera", owner: self, options: nil)
        guard let content = contentViewCamera else { return }
        content.frame = self.bounds
        content.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.addSubview(content)
    }
    
    @IBAction func confirmButtonAction(_ sender: UIButton) {
        delegate?.confirmButton()
    }
    
}
