//
//  NapoliScannerVCExt.swift
//  Steward Check-in
//
//  Created by Youssef on 05/02/2020.
//  Copyright © 2020 KM, Abhilash. All rights reserved.
//

import UIKit

extension ScannerViewController {
    
    internal func setDataLog(user: Steward,checkTime: Date,event: Int) {
            DataManager.shared.addLog(forEvent: event, cf: user.cf!, withTimestamp: checkTime, andStatus: modeType == .checkIn ? "OK" : "OUT")
    }
    
    internal func setDetailView(steward: Steward) {
    }
    
    internal func setDetailViewCamera(steward: Steward) {
    }
}
