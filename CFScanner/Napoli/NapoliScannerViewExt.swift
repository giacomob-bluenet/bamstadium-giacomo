//
//  ScannerView.swift
//  Steward Check-in
//
//  Created by Youssef on 30/01/2020.
//  Copyright © 2020 KM, Abhilash. All rights reserved.
//

import Foundation
import AVFoundation

extension ScannerView {
    func setCode(metadataOutput: AVCaptureMetadataOutput){
        metadataOutput.metadataObjectTypes = [.code39,.code128,.qr]
    }
    
    func findCode(withCode code:String?,withType type: AVMetadataObject.ObjectType) -> String? {
        return code
    }
}
