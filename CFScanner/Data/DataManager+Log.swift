//
//  DataManager+Log.swift
//  bluecode-reader
//
//  Created by Alessandro de Peppo on 20/06/2019.
//  Copyright © 2019 Bluenet. All rights reserved.
//

import Foundation
import CoreData

extension DataManager {
        
    /**
     Fetch not synced logs
     - Parameter event: The eventId to sync. If nil, any events wil be synced
     - Returns: true if card is blacklisted, false otherwise
     */
    func fetchLogs(forEvent event: Int?, withContext ctx: NSManagedObjectContext) -> [Log]? {
        let request : NSFetchRequest<Log> = Log.fetchRequest()
        request.fetchLimit = DataManager.fetchLimit
        let syncKeyPredicate = NSPredicate(format: "sync = 0")
        if let event = event {
            let eventKeyPredicate = NSPredicate(format: "event = %d", event)
            let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [syncKeyPredicate, eventKeyPredicate])
            request.predicate = andPredicate
        } else {
            request.predicate = syncKeyPredicate
        }
        
        do {
            let logs = try ctx.fetch(request)
            return logs
        } catch {
            print("Failed to fetch logs: \(error)")
        }
        return nil
    }
    
    func addLog(forEvent event: Int, cf :String, withTimestamp checkTime: Date, andStatus status: String) {
        let ctx = persistentContainer.viewContext
        let log = Log(context: ctx)
        log.timestamp = checkTime
        log.cf = cf
        log.event = Int32(event)
        log.status = status
        log.sync = false
        
        do {
            try ctx.save()
        } catch {
            print("Failure to save context: \(error)")
        }
    }
    
    func addLog(forEvent event: Int, cf :String, withTimestamp checkTime: Date, andExtra extra: String, andStatus status: String) {
        let ctx = persistentContainer.viewContext
        let log = Log(context: ctx)
        log.timestamp = checkTime
        log.cf = cf
        log.event = Int32(event)
        log.extra = extra
        log.status = status
        log.sync = false
        
        do {
            try ctx.save()
        } catch {
            print("Failure to save context: \(error)")
        }
    }
    
    func lastLogId(forEvent event: Int?, withContext ctx: NSManagedObjectContext) -> Int {
        let request: NSFetchRequest<Log> = Log.fetchRequest()
        request.fetchLimit = 1
        
        let predicate = NSPredicate(format: "id == max(id)")
        request.predicate = predicate
        
        do {
            if let result = try ctx.fetch(request).first {
                return Int(result.id)
            }
        } catch {
            print("Unresolved error in retrieving max id value \(error)")
        }
        return 0
    }
    
    /**
     Remove all log in db
     */
    func removeAllLog(withContext ctx: NSManagedObjectContext){
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Log")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        do {
            try ctx.execute(deleteRequest)
            try ctx.save()
        } catch {
            print ("error")
        }
    }
    
}
