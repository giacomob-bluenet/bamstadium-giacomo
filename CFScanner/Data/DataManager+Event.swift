//
//  DataManager+Event.swift
//  bluecode-reader
//
//  Created by Youssef on 20/01/2020.
//  Copyright © 2019 Bluenet. All rights reserved.
//

import Foundation
import CoreData

extension DataManager {
        
    /**
         Get event from database using viewContext
         - Parameter card: The code to be checked
         - Parameter event: The eventId to be checked
         - Returns: event entry
         */
        func getEvent(forEvent event: Int) -> Event? {
            let conetxt = persistentContainer.viewContext
            return getEventWithContext(conetxt, forEvent: event)
        }
    
    /**
        Get event from database
        - Parameter context: NSManagedObhject context
        - Parameter code: The code to be checked
        - Parameter event: The eventId to be checked
        - Returns: event entry
        */
       func getEventWithContext(_ context:  NSManagedObjectContext, forEvent event: Int) -> Event? {
           return getGenericEventWithContext(context, forEvent: event)
       }
    
    /**
         Get event from database
         - Parameter context: NSManagedObhject context
         - Parameter event: The eventId to be checked
         - Returns: event entry
         */
        func getGenericEventWithContext(_ context:  NSManagedObjectContext, forEvent event: Int) -> Event? {
            let request : NSFetchRequest<Event> = Event.fetchRequest()
            let eventKeyPredicate = NSPredicate(format: "id = %d", event)
            request.predicate = eventKeyPredicate
            
            do {
                let items = try context.fetch(request)
                if items.count > 0  {
                    return items[0]
                } else {
                    return nil;
                }
            } catch {
                print("Failed to fetch items: \(error)")
            }
            return nil
        }
        
        /**
         Get events from database
         - Returns: Last 5 event entry
         */
        func getEvents() -> [Event]? {
            let context = persistentContainer.viewContext
            let futureDate = getFutureDate()
            let request : NSFetchRequest<Event> = Event.fetchRequest()
            //let sort = NSSortDescriptor(key: "startTime", ascending: false)
            let sort = NSSortDescriptor(key: "startTime", ascending: true)
            request.predicate = NSPredicate(format: "startTime <= %@", futureDate! as NSDate)
            //request.fetchLimit = 5
            request.sortDescriptors = [sort]

            do {
                let items = try context.fetch(request)
                if items.count > 0  {
                    return items
                } else {
                    return nil;
                }
            } catch {
                print("Failed to fetch items: \(error)")
            }
            return nil
        }
    
    private func getFutureDate() -> Date? {
          let daysToAdd = 30
          let currentDate = Date()
          var dateComponent = DateComponents()
          
          dateComponent.day = daysToAdd
                          
          return Calendar.current.date(byAdding: dateComponent, to: currentDate)
    }
    
    /**
     Remove all event in db
     */
    func removeAllEvent(withContext ctx: NSManagedObjectContext){
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Event")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        do {
            try ctx.execute(deleteRequest)
            try ctx.save()
        } catch {
            print ("error")
        }
    }
    
}
