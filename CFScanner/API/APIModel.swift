//
//  APIModel.swift
//  CFScanner
//
//  Created by Alessandro de Peppo on 20/06/2019.
//  Copyright © 2019 Bluenet. All rights reserved.
//

import Foundation

struct LogApi : Codable {
    let id: Int?
    let code : String
    let type : String
    let codeType : String
    let extra : String?
    let status : String
    let ts : Date
}

struct TicketApi : Codable {
    let code : String
    let ts : String?
    let cardType: String
    let area : String?
    let sector : String?
    let used: Bool
    let valid: Bool
    let givenName: String?
    let familyName: String?
    let dob: String?
    let pob: String?
    let extra: String?
    let row: String?
    let seat: String?
}

struct EventsApi : Codable {
    let id : Int
    let name : String?
    let startDate: String?
}


struct ResponseApi: Codable {
    let id: String
}

