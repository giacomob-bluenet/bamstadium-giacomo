//
//  AppDelegate.swift
//  CFScanner
//
//  Created by Alessandro de Peppo on 20/06/2019.
//  Copyright © 2019 Bluenet. All rights reserved.
//


import UIKit
import SwiftUI

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var loadLogsTimer : DispatchSourceTimer?
    var loadStewardTimer : DispatchSourceTimer?
    var syncLogsTimer : DispatchSourceTimer?
    var arrSectors : [String] = []
    var arrAreasAndSectors = [String : [String]]()
    var arrAreasAndSectorsFinal = [String : [String]]()
    let viewHosting = UIHostingController(rootView: LoginView())
    var event : Int?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        if UserDefaults.standard.value(forKey: firstOpenApp) as? String != nil{
            print("save preferences firstOpenApp")
        }else{
            self.setFirstOpenAppAndSetModeCamera()
        }
        if UserDefaults.standard.value(forKey: usernameApp) as? String != nil{
            print("user saved")
            //check if areas and events have been previously saved
            if UserDefaults.standard.value(forKey: areasAndSectors) != nil{
                let arr = UserDefaults.standard.value(forKey: areasAndSectors) as! [String : [String]]
                self.arrAreasAndSectors = arr
                self.arrAreasAndSectorsFinal = arr
            }
            //set Credentials and load Events
            self.setCredentialsAndLoadEvents()
        }else{
            //load view Login
            self.loadLoginView()
        }

        return true
    }
    
    func startBackgroundProcs(forEvent event : Int) {
        if let syncLogsTimer = syncLogsTimer, (!syncLogsTimer.isCancelled) {
            return
        }
        print("Starting Background Processes form event \(event)")
        
        loadStewardTimer = DispatchSource.makeTimerSource(flags: [], queue: .main)
        loadStewardTimer?.schedule(deadline: .now(), repeating: 20.0)
        loadStewardTimer?.setEventHandler(handler: {
            APIManager.shared.loadStewards(forEvent: event)
            APIManager.shared.addStewards(forEvent: event)
            
        })
        loadStewardTimer?.resume()
        
        loadLogsTimer = DispatchSource.makeTimerSource(flags: [], queue: .main)
        loadLogsTimer?.schedule(deadline: .now()+12.0, repeating: 20.0)
        loadLogsTimer?.setEventHandler(handler: {
            APIManager.shared.loadLogs(forEvent: event)
            
        })
        loadLogsTimer?.resume()
        
        syncLogsTimer = DispatchSource.makeTimerSource(flags: [], queue: .main)
        syncLogsTimer?.schedule(deadline: DispatchTime.now()+6.0, repeating: 20.0)
        syncLogsTimer?.setEventHandler(handler: {
            APIManager.shared.addLogs(forEvent: event)
        })
        syncLogsTimer?.resume()
        
        /*DispatchQueue.global().asyncAfter(deadline: .now()) { [self] in
            loadStewardTimer = DispatchSource.makeTimerSource(flags: [], queue: .main)
            loadStewardTimer?.schedule(deadline: .now(), repeating: 20.0)
            loadStewardTimer?.setEventHandler(handler: {
                APIManager.shared.loadStewards(forEvent: event)
                APIManager.shared.addStewards(forEvent: event)
                
            })
            loadStewardTimer?.resume()
            print("First")
            DispatchQueue.global().asyncAfter(deadline: .now()) {
                loadLogsTimer = DispatchSource.makeTimerSource(flags: [], queue: .main)
                loadLogsTimer?.schedule(deadline: .now()+12.0, repeating: 20.0)
                loadLogsTimer?.setEventHandler(handler: {
                    APIManager.shared.loadLogs(forEvent: event)
                    
                })
                loadLogsTimer?.resume()
                print("Second")
                DispatchQueue.main.async {
                    syncLogsTimer = DispatchSource.makeTimerSource(flags: [], queue: .main)
                    syncLogsTimer?.schedule(deadline: DispatchTime.now()+6.0, repeating: 20.0)
                    syncLogsTimer?.setEventHandler(handler: {
                        APIManager.shared.addLogs(forEvent: event)
                    })
                    syncLogsTimer?.resume()
                }
            }
        }*/
            
    }
    
    func stopBackgrounProcs() {
        print("Stopping Background Processes")
        syncLogsTimer?.cancel()
        loadStewardTimer?.cancel()
        loadLogsTimer?.cancel()

        syncLogsTimer = nil
        loadStewardTimer = nil
        loadLogsTimer = nil
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        UserDefaults.standard.set(false, forKey: "Light")
        Torch().turnOffTorch()

    }
    
    //Function save firstOpenApp and set Mode Camera
    func setFirstOpenAppAndSetModeCamera(){
        UserDefaults.standard.set("0", forKey: firstOpenApp)
        SettingsManager.sharedInstance.saveModeCheck = "ModeCheck0"
    }
    
    //Function reset UserDefaults
    func resetUserDefaults() {
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: usernameApp)
        userDefaults.removeObject(forKey: passwordApp)
        userDefaults.removeObject(forKey: areasAndSectors)
        UserDefaults.standard.set(0, forKey: "event")
        userDefaults.synchronize()
    }
    
    //load view Login
    func loadLoginView(){
        viewHosting.modalPresentationStyle = .fullScreen
        DispatchQueue.delay(.milliseconds(300)) { [self] in
            self.window?.rootViewController?.present(viewHosting, animated: true, completion: nil)
        }
    }
    
    //set Credentials and Download Events
    func setCredentialsAndLoadEvents(){
        
        let username = UserDefaults.standard.value(forKey: usernameApp) as! String
        let password = UserDefaults.standard.value(forKey: passwordApp) as! String
        let cred = Credentials(username: username, password: password)
        let loginString = "\(cred.username):\(cred.password)"
        guard let loginData = loginString.data(using: String.Encoding.utf8) else {
            APIManager.shared.base64Credentials = nil
            return
        }
        
        APIManager.shared.base64Credentials = loginData.base64EncodedString()
        DispatchQueue.delay(.milliseconds(100)) {
            APIManager.shared.loadEvents(onSuccess: { checkEvent  in
                DispatchQueue.main.async {
                    print("loadEvents \(checkEvent)")
                }
            },onFailure: { strError  in
                DispatchQueue.main.async {
                    print(strError)
                    print("loadEvents Error")
                }
            })
        }
    }


}

