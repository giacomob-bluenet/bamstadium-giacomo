//
//  CsvCreate.swift
//  Poll Check-in
//
//  Created by Youssef on 25/11/2019.
//  Copyright © 2019 KM, Abhilash. All rights reserved.
//

import Foundation
import UIKit

class CsvCreate  {
    
    /// Create single string to use in CSV file creation
    ///
    class func createExportString(event: Int) -> String {
        //Write first row ( Creation of column name )
        var export : String = getHeader();
        
        //Create
        if let checked = DataManager.shared.findSteward(forEvent: event){
            for item in checked {
                export += getFormattedRow(for: item)
            }
         }
         return export
     }

    /// Create a CSV file
    ///
    ///- Parameters:
    ///  - exportString: content of the csv
    ///  - viewController: view in which to display actionsheet
    ///
    class func saveAndExport(exportString: String, viewController : UIViewController, eventName: String) {
         //Create temporary folder to save the CSV file
         let exportFilePath = NSTemporaryDirectory() + "/report-steward_" + eventName + "_" + DateFormatter.dtfCsv.string(from: Date()) + ".csv"
        
         //Create URL to that specific temporary folder
         let exportFileURL = NSURL(fileURLWithPath: exportFilePath)
        
         //Create CSV
         FileManager.default.createFile(atPath: exportFilePath, contents: NSData() as Data, attributes: nil)
         var fileHandle: FileHandle? = nil
         do {
            fileHandle = try FileHandle(forWritingTo: exportFileURL as URL)
         } catch {
             print("Error with fileHandle")
         }
         
         if fileHandle != nil {
             fileHandle!.seekToEndOfFile()
             let csvData = exportString.data(using: String.Encoding.utf8, allowLossyConversion: false)
             fileHandle!.write(csvData!)
             
             fileHandle!.closeFile()
             
             //Get file to export
             let firstActivityItem = NSURL(fileURLWithPath: exportFilePath)
            
             //Create ViewController that handle export
             let activityViewController : UIActivityViewController = UIActivityViewController(
                 activityItems: [firstActivityItem], applicationActivities: nil)
             
             //Specific types of actions that should not be displayed to the user
             activityViewController.excludedActivityTypes = [
                UIActivity.ActivityType.assignToContact,
                UIActivity.ActivityType.saveToCameraRoll,
                UIActivity.ActivityType.postToFlickr,
                UIActivity.ActivityType.postToVimeo,
                UIActivity.ActivityType.postToTencentWeibo,
                UIActivity.ActivityType.openInIBooks,
                UIActivity.ActivityType.copyToPasteboard,
                UIActivity.ActivityType.message,
                UIActivity.ActivityType.init(rawValue: "com.apple.reminders.RemindersEditorExtension"),
                UIActivity.ActivityType.init(rawValue: "com.apple.mobilenotes.SharingExtension")
             ]
             
            viewController.present(activityViewController, animated: true, completion: nil)
         }
     }

    /// Create a CSV file
    ///
    ///- Parameters:
    ///  - exportString: content of the csv
    ///  - viewController: view in which to display actionsheet
    ///
    class func saveAndExportinFileShare(exportString: String, eventName: String) {
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        var documentsDirectory : String
        documentsDirectory = (paths[0] as String) + "/report-steward_" + eventName + "_" + DateFormatter.dtfCsv.string(from: Date()) + ".csv"
        print(documentsDirectory)
        
        //Create URL to that specific folder
        let exportFileURL = NSURL(fileURLWithPath: documentsDirectory)
        
        //Create CSV
        FileManager.default.createFile(atPath: documentsDirectory, contents: NSData() as Data, attributes: nil)
        var fileHandle: FileHandle? = nil
        do {
           fileHandle = try FileHandle(forWritingTo: exportFileURL as URL)
        } catch {
            print("Error with fileHandle")
        }
        
        if fileHandle != nil {
            fileHandle!.seekToEndOfFile()
            let csvData = exportString.data(using: String.Encoding.utf8, allowLossyConversion: false)
            fileHandle!.write(csvData!)

            fileHandle!.closeFile()
        }
    }
}
