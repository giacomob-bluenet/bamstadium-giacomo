//
//  Torch.swift
//  Steward Check-in
//
//  Created by Youssef on 16/10/2019.
//  Copyright © 2019 KM, Abhilash. All rights reserved.
//

import AVFoundation
import UIKit

class Torch {
    
    // MARK: - Init
    
    init() { }
    
    func torchAction() {
        if( UserDefaults.standard.bool(forKey: "Light") ){
            UserDefaults.standard.set(false, forKey: "Light")
            toggleOffFlash()
        }else{
           UserDefaults.standard.set(true, forKey: "Light")
           toggleOnFlash()
        }
    }
    
    func turnOffTorch() {
           toggleOffFlash()
    }
    
    func turnOnTorch() {
        if( UserDefaults.standard.bool(forKey: "Light") ){
            UserDefaults.standard.set(true, forKey: "Light")
            toggleOnFlash()
        }
    }
    
    // MARK: - toggle Function
    
    private func toggleOnFlash() {
        guard let device = AVCaptureDevice.default(for: AVMediaType.video) else { return }
        guard device.hasTorch else { return }
        
        do {
            try device.lockForConfiguration()
            
            if !(device.torchMode == AVCaptureDevice.TorchMode.on) {
                do {
                    try device.setTorchModeOn(level: 1.0)
                } catch {
                    print(error)
                }
            }
            
            device.unlockForConfiguration()
        } catch {
            print(error)
        }
    }
    
    private func toggleOffFlash(){
        guard let device = AVCaptureDevice.default(for: AVMediaType.video) else { return }
        guard device.hasTorch else { return }
        
        do {
            try device.lockForConfiguration()
            
            if !(device.torchMode == AVCaptureDevice.TorchMode.off) {
                device.torchMode = .off
            }
            
            device.unlockForConfiguration()
        } catch {
            print(error)
        }
    }
}
