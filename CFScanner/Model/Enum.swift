//
//  Enum.swift
//  Steward Check-in
//
//  Created by Youssef on 27/01/2020.
//  Copyright © 2020 KM, Abhilash. All rights reserved.
//

import Foundation

enum ModeType: Int {
    // app setted in check-in mode
    case checkIn = 0
    // app setted in check-out mode
    case checkOut = 1
}

enum CheckIn: Int16 {
    // no check-in
    case noCheck = 0
    // local check-in
    case local = 1
    // external check-in
    case external = 2
}

enum CheckOut: Int16 {
    // no check-in
    case noCheck = 0
    // local check-out
    case local = 1
    // external check-out
    case external = 2
}

enum List: Int {
    // no check
    case unChecked = 0
    // checked
    case Checked = 1
}


enum ModeCheck: String {
    case checkCamera = "ModeCheck0"
    case checkInfrared = "ModeCheck1"
}
