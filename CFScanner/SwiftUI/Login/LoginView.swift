//
//  LoginView.swift
//  Steward Check-in
//
//  Created by Giacomo Boemio on 28/10/21.
//  Copyright © 2021 Bluenet srl. All rights reserved.
//

import SwiftUI

let lightGreyColor = Color(red: 239.0/255.0, green: 243.0/255.0, blue: 244.0/255.0, opacity: 1.0)

struct LoginView : View {

    @State var username: String = ""
    @State var password: String = ""
    @State var changeStatus: Bool = false
    @State var stringError: String = ""
    @State var showAlert = false
    @State var showAlertConnection = false
    @State var loadingSpinner: Bool = false

    let networkReachability = NetworkReachability()
    
    var body: some View {
        LoadingView(isShowing: .constant(self.loadingSpinner)) {
            VStack {
                UserImage()
                TextField(NSLocalizedString("username", comment: ""), text: $username)
                    .padding()
                    .background(lightGreyColor)
                    .cornerRadius(5.0)
                    .padding(.bottom, 20)
                    .modifier(TextFieldClearButton(text: $username))
                SecureField(NSLocalizedString("password", comment: ""), text: $password)
                    .padding()
                    .background(lightGreyColor)
                    .cornerRadius(5.0)
                    .padding(.bottom, 20)
                    .modifier(TextFieldClearButton(text: $password))
                Button(action: {
                    let checkConnection = self.networkReachability.checkConnection()
                    if(checkConnection){
                        self.loginUser(username: username, password: password)
                    }else{
                        self.showAlert = true
                        self.stringError = NSLocalizedString("error_connection", comment: "")
                    }
                }) {
                   LoginButtonContent()
                }.alert(isPresented: $showAlert) {
                    if(self.stringError.isEmpty){
                        return Alert(
                            title: Text(NSLocalizedString("error", comment: "")),
                            message: Text(NSLocalizedString("error_username_passowrd", comment: ""))
                        )
                    }else{
                        return Alert(
                            title: Text(NSLocalizedString("error", comment: "")),
                            message: Text(NSLocalizedString(self.stringError, comment: ""))
                        )
                    }
                }
            }.onAppear(perform: {
                UIApplication.shared.addTapGestureRecognizer()
            })
            .padding()
        }
        
    }
    
    private func loginUser(username: String, password: String){
        if(username.isEmpty || password.isEmpty){
            self.showAlert = true
            self.stringError = ""
        }else{
            DispatchQueue.global().asyncAfter(deadline: .now() + 0.1) { [self] in
                self.loadingSpinner = true
                DispatchQueue.delay(.milliseconds(100)) {
                    APIManager.shared.loginSteward(username: username, password: password, onSuccess: { json  in
                        DispatchQueue.main.async {
                            self.loadingSpinner = false
                            UserDefaults.standard.set(username, forKey: usernameApp)
                            UserDefaults.standard.set(password, forKey: passwordApp)
                            UserDefaults.standard.synchronize()
                            self.username = ""
                            self.password = ""
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.setCredentialsAndLoadEvents()
                            DispatchQueue.delay(.milliseconds(500)) {
                                appDelegate.viewHosting.dismiss(animated: true, completion: nil)
                            }
                        }
                    }, onFailure: { strError  in
                        RetryManager.sharedInstance.startRetry(retryCount: 3) { (result) in
                            if(result){
                                DispatchQueue.delay(.milliseconds(1500)) {
                                    self.loginUser(username: username, password: password)
                                }
                            }else{
                                DispatchQueue.main.async {
                                    print(strError)
                                    self.loadingSpinner = false
                                    self.showAlert = true
                                    self.stringError = strError
                                }
                            }
                        }
                    })
                }
            }
        }
    }

}

#if DEBUG
struct ContentView_Previews : PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
#endif

struct UserImage : View {
    var body: some View {
        return Image("LogoBluenet")
            .resizable()
            .aspectRatio(contentMode: .fill)
            .frame(width: 150, height: 150)
            .clipped()
            .padding(.bottom, 20)
    }
}

struct LoginButtonContent : View {
    var body: some View {
        return Text(NSLocalizedString("login", comment: "").uppercased())
            .font(.headline)
            .foregroundColor(.white)
            .padding()
            .frame(width: 220, height: 60)
            .background(Color.green)
            .cornerRadius(15.0)
    }
}


struct TextFieldClearButton: ViewModifier {
    @Binding var text: String
    
    func body(content: Content) -> some View {
        HStack {
            content
            if !text.isEmpty {
                Button(
                    action: { self.text = "" },
                    label: {
                        Image(systemName: "delete.left")
                            .foregroundColor(Color(UIColor.opaqueSeparator))
                    }
                )
            }
        }
    }
}

