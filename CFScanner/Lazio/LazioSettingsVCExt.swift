//
//  LazioSettingsVCExt.swift
//  Steward Check-in
//
//  Created by Youssef on 05/02/2020.
//  Copyright © 2020 KM, Abhilash. All rights reserved.
//

import UIKit

extension SettingsViewController {
    
    internal func setSection() -> [String] {
        return ["List","Mode","Export"]
    }
    
    internal func saveSettings() {
        if  UserDefaults.standard.integer(forKey: "event") != currentEvent  {
            UserDefaults.standard.set(0, forKey: "Mode")
            UserDefaults.standard.set(currentEvent, forKey: "event")
        } else {
            UserDefaults.standard.set(lastRowForSection[1], forKey: "Mode")
        }
    }
    
    func numberOfRowsInSection(withSection section: String) -> Int {
        switch section {
        case "Export":
            return 2
        case "List":
            return events?.count ?? 0
        case "Mode":
            return 2
        default:
            return 1
        }
    }
    
    internal func handleRowSelection(withSection section: String, withTableView tableView: UITableView, withIndexPath indexPath: IndexPath) {
        switch section {
           case "Export":
                handleExport(tableView: tableView, indexPath: indexPath)
           case "List":
                handleList(tableView: tableView, indexPath: indexPath)
           case "Mode":
                tableView.cellForRow(at: NSIndexPath.init(row: lastRowForSection[1], section: 1) as IndexPath)?.accessoryType = .none
                if let cell = tableView.cellForRow(at: indexPath) {
                    lastRowForSection[1] = indexPath.row
                    cell.accessoryType = .checkmark
                    UserDefaults.standard.set(indexPath.row, forKey: "Mode")
                }
           default: break
        }
    }
    
    internal func setCell(withSection section: String,withCell cell:UITableViewCell,withIndexPath indexPath: IndexPath) -> UITableViewCell {
        switch section {
           case "Export":
            setExportCell(withCell: cell, withIndexPath: indexPath)
                return cell
           case "List":
            setListCell(withCell: cell, withIndexPath: indexPath)
               return cell
           case "Mode":
                cell.textLabel?.text = NSLocalizedString("Mode\(indexPath.row)", comment: "")
                if UserDefaults.standard.integer(forKey: "Mode") == indexPath.row {
                    lastRowForSection[1] = indexPath.row
                    tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
                    cell.accessoryType = .checkmark
                }
                return cell
           default:
               return cell
        }
    }
    
}
