//
//  LazioScannerVCExt.swift
//  Steward Check-in
//
//  Created by Youssef on 05/02/2020.
//  Copyright © 2020 KM, Abhilash. All rights reserved.
//

import UIKit

extension ScannerViewController {
    
    internal func setDataLog(user: Steward,checkTime: Date,event: Int) {
        if let newCode = detailView.sc?.text, newCode != user.extra {
            DataManager.shared.addLog(forEvent: event, cf: user.cf! , withTimestamp: checkTime,andExtra: newCode, andStatus: modeType == .checkIn ? "OK" : "OUT")
            user.extra = newCode
        } else {
            DataManager.shared.addLog(forEvent: event, cf: user.cf!, withTimestamp: checkTime, andStatus: modeType == .checkIn ? "OK" : "OUT")
        }
    }
    
    internal func setDetailView(steward: Steward) {
        detailView.sector?.text = steward.sector
        setKeyboardToolbar()

        if(steward.extra != "warn" && steward.extra != "cu") {
            self.detailView.sc?.text = steward.extra
        }
        
        if (modeType == .checkIn ? steward.checkTime : steward.checkOutTime) != nil {
            self.detailView.sc?.isEnabled = false
        } else {
            self.detailView.sc?.isEnabled = modeType == .checkIn && steward.area == "LAZIO"
        }
    }
    
    internal func setDetailViewCamera(steward: Steward) {
        detailViewCamera.sector?.text = steward.sector
        setKeyboardToolbar()

        if(steward.extra != "warn" && steward.extra != "cu") {
            self.detailViewCamera.sc?.text = steward.extra
        }
        
        if (modeType == .checkIn ? steward.checkTime : steward.checkOutTime) != nil {
            self.detailViewCamera.sc?.isEnabled = false
        } else {
            self.detailViewCamera.sc?.isEnabled = modeType == .checkIn && steward.area == "LAZIO"
        }
    }
    
    func setKeyboardToolbar() {
        let numberToolbar: UIToolbar = UIToolbar()
        
        numberToolbar.barStyle = UIBarStyle.default
        numberToolbar.items=[
            UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: NSLocalizedString("Done", comment: ""), style: UIBarButtonItem.Style.done, target: self, action: #selector(apply))
        ]

        numberToolbar.sizeToFit()
        if(self.modeCheck == ModeCheck.checkInfrared.rawValue){
            detailView.sc.inputAccessoryView = numberToolbar
        }else{
            detailViewCamera.sc.inputAccessoryView = numberToolbar
        }
    }

    @objc private func apply () {
        if(self.modeCheck == ModeCheck.checkInfrared.rawValue){
            detailView.sc.resignFirstResponder()
        }else{
            detailViewCamera.sc.inputAccessoryView = numberToolbar
        }
    }
    
}
