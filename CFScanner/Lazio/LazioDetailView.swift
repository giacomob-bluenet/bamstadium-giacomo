//
//  LazioDetailView.swift
//  Steward Check-in
//
//  Created by Youssef on 05/02/2020.
//  Copyright © 2020 KM, Abhilash. All rights reserved.
//

import UIKit

public class DetailView: UIView {
    
    @IBOutlet var DetailView: DetailView!
    @IBOutlet weak var sector: UILabel!
    @IBOutlet weak var cognome: UILabel!
    @IBOutlet weak var dob: UILabel!
    @IBOutlet weak var checked: UILabel!
    @IBOutlet weak var sc: UITextField!
    @IBOutlet weak var nome: UILabel!
    @IBOutlet weak var confButton: UIButton!
    
    weak var delegate : DetailViewDelegate?

    @IBOutlet weak var cf: UILabel!
    override init(frame: CGRect) {

        // Call super init

        super.init(frame: frame)

        // 3. Setup view from .xib file

        configureXIB(named: "LazioDetailView")

    }

    required init?(coder aDecoder: NSCoder) {

        // 1. setup any properties here

        // 2. call super.init(coder:)

        super.init(coder: aDecoder)

        // 3. Setup view from .xib file

        configureXIB(named: "LazioDetailView")

    }
    
}
