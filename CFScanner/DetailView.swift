//
//  DetailView.swift
//  Steward Check-in
//
//  Created by Youssef on 05/02/2020.
//  Copyright © 2020 KM, Abhilash. All rights reserved.
//

import UIKit

/// Delegate callback for the ScannerView.
protocol DetailViewDelegate: class {
    func confirmButton()
}

extension DetailView {

    
    
    func configureXIB(named: String) {

        Bundle.main.loadNibNamed(named, owner: self, options: nil)
        // use bounds not frame or it’ll be offset

       DetailView.frame = bounds
        
        // Make the flexible view

        DetailView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]

        // Adding custom subview on top of our view (over any custom drawing > see note below)

        addSubview(DetailView)

    }
    
    /*@IBAction func confirmButtonAction(_ sender: UIButton) {
        delegate?.confirmButton()
    }*/
}


